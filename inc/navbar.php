<?php
/**
 * Created by PhpStorm.
 * User: Harsh
 * Date: 19-05-2018
 * Time: 15:28
 */
function navbar(){
    ?>


    <header id="header"><!--header-->
        <div class="header_top"><!--header_top-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contactinfo">
                            <ul class="nav nav-pills">
                                <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="social-icons pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header_top-->

        <div class="header-middle"><!--header-middle-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="logo pull-left">
                            <a href="<?php echo HOME . 'index.php' ?>"><img src="<?php echo HOME . 'images/home/logo-black.png' ?>" style="height:60px;" alt="_logo" /></a>
                        </div>
                        <!-- <div class="btn-group pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                    USA
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Canada</a></li>
                                    <li><a href="#">UK</a></li>
                                </ul>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                    DOLLAR
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Canadian Dollar</a></li>
                                    <li><a href="#">Pound</a></li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                    <div class="col-sm-5">
                    </div>
                    <div class="col-sm-5">
                        <div class="shop-menu pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-user"></i> Account</a></li>
                                <li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
                                <li><a href="<?php echo HOME . 'checkout.php' ?>"><i class="fa fa-crosshairs"></i> Checkout</a></li>
                                <li><a href="<?php echo HOME . 'cart.php' ?>"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                                <li><a href="<?php echo HOME . 'login.php' ?>"><i class="fa fa-lock"></i> Login</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-middle-->
<!--header-bottom-->
        <div class="header-bottom pd_bt0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
<!--
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="mainmenu pull-left">
                            <ul class="nav navbar-nav collapse navbar-collapse">
                                
                                
                            </ul>
                        </div>
-->
                    </div>
                    <div class="col-sm-12">
                        <section class="search-form">
                            <form action="" method="GET" name="search" role="search">
                              <p class="inp-wrap search-wrap">
                                <label for="search-field" class="search-label grid-25">Find</label>
                                <input type="search" name="search-term" id="search-field" class="grid-75" placeholder="e.g. pizza, pet supplies" /></p>
                              <p class="inp-wrap cat-wrap">
                                <label for="categories" class="grid-20">in</label>
                                <select name="search categories" id="categories" class="grid-80">
                                  <option value="newyork" selected>New York</option>
                                  <option value="chicago">Chicago</option>
                                  <option value="losangeles">Los Angeles</option>
                                  <option value="seattle">Seattle</option>
                                  <option value="dallas">Dallas</option>
                                  <option value="boston">Boston</option>
                                  <option value="sanfran">San Francisco</option>
                                </select>
                              </p>
                              <p class="inp-wrap submit-wrap">
                                <button class="grid-100 btn">
                                  <span class="search-icon-container"><?xml version="1.0" encoding="utf-8"?>
                                    <i class="fas fa-search"></i> 
                                  </span>
                                  Search
                                </button>
                              </p>
                            </form>
                          </section> 
                    </div>
                </div>
            </div>
        </div>
        <!--/header-bottom-->
        <div class="header-bottom pad_tp0">
            <div class="container">
            <nav class="navbar marg_bt0 navbar-inverse">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo HOME . 'index.php' ?>">Home</a>
                   
                </div>
                
                <div class="collapse navbar-collapse js-navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown mega-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Personalised Gifts <span class="caret"></span></a>				
                            <ul class="dropdown-menu mega-dropdown-menu">
                                <li class="col-sm-3">
                                <ul>
                                        <li class="dropdown-header">Much more</li>
                                        <li><a href="#">Easy to Customize</a></li>
                                        <li><a href="#">Calls to action</a></li>
                                        <li><a href="#">Custom Fonts</a></li>
                                        <li><a href="#">Slide down on Hover</a></li>                         
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Features</li>
                                        <li><a href="#">Auto Carousel</a></li>
                                        <li><a href="#">Carousel Control</a></li>
                                        <li><a href="#">Left & Right Navigation</a></li>
                                        <li><a href="#">Four Columns Grid</a></li>
                                        <li class="divider"></li>
                                        <li class="dropdown-header">Fonts</li>
                                        <li><a href="#">Glyphicon</a></li>
                                        <li><a href="#">Google Fonts</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Plus</li>
                                        <li><a href="#">Navbar Inverse</a></li>
                                        <li><a href="#">Pull Right Elements</a></li>
                                        <li><a href="#">Coloured Headers</a></li>                            
                                        <li><a href="#">Primary Buttons & Default</a></li>							
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Much more</li>
                                        <li><a href="#">Easy to Customize</a></li>
                                        <li><a href="#">Calls to action</a></li>
                                        <li><a href="#">Custom Fonts</a></li>
                                        <li><a href="#">Slide down on Hover</a></li>                         
                                    </ul>
                                </li>
                            </ul>				
                        </li>
                        <li class="dropdown mega-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Corporate Gifts <span class="caret"></span></a>				
                            <ul class="dropdown-menu mega-dropdown-menu">
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Features</li>
                                        <li><a href="#">Auto Carousel</a></li>
                                        <li><a href="#">Carousel Control</a></li>
                                        <li><a href="#">Left & Right Navigation</a></li>
                                        <li><a href="#">Four Columns Grid</a></li>
                                        <li class="divider"></li>
                                        <li class="dropdown-header">Fonts</li>
                                        <li><a href="#">Glyphicon</a></li>
                                        <li><a href="#">Google Fonts</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Plus</li>
                                        <li><a href="#">Navbar Inverse</a></li>
                                        <li><a href="#">Pull Right Elements</a></li>
                                        <li><a href="#">Coloured Headers</a></li>                            
                                        <li><a href="#">Primary Buttons & Default</a></li>							
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Much more</li>
                                        <li><a href="#">Easy to Customize</a></li>
                                        <li><a href="#">Calls to action</a></li>
                                        <li><a href="#">Custom Fonts</a></li>
                                        <li><a href="#">Slide down on Hover</a></li>                         
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                <ul>
                                        <li class="dropdown-header">Much more</li>
                                        <li><a href="#">Easy to Customize</a></li>
                                        <li><a href="#">Calls to action</a></li>
                                        <li><a href="#">Custom Fonts</a></li>
                                        <li><a href="#">Slide down on Hover</a></li>                         
                                    </ul>
                                </li>
                            </ul>				
                        </li>
                        <li><a href="#">Store locator</a></li>
                        <li><a class="navbar-brand" href="#">Home</a></li>
                        <li class="dropdown"><a class="navbar-brand" href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="<?php echo HOME . 'shop.php' ?>">Products</a></li>
                                        <li><a href="<?php echo HOME . 'product-details.php' ?>">Product Details</a></li>
                                        <li><a href="<?php echo HOME . 'checkout.php' ?>">Checkout</a></li>
                                        <li><a href="<?php echo HOME . 'cart.php' ?>">Cart</a></li>
                                        <li><a href="<?php echo HOME . 'login.php' ?>">Login</a></li>
                                    </ul>
                        </li>
                        <li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="<?php echo HOME . 'blog.php' ?>">Blog List</a></li>
                                        <li><a href="<?php echo HOME . 'blog-single.php' ?>">Blog Single</a></li>
                                    </ul>
                                </li>
                        <li><a href="<?php echo HOME . '404.php' ?>">404</a></li>
                        <li><a href="<?php echo HOME . 'contact-us.php' ?>">Contact</a></li>
                        
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                    <li><p class="navbar-text">Already have an account?</p></li>
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                        <ul id="login-dp" class="dropdown-menu">
                            <li>
                                <div class="row">
                                        <div class="col-md-12">
                                            Login via
                                            <div class="social-buttons">
                                                <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                                <a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
                                            </div>
                                            or
                                            <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                                                    <div class="form-group">
                                                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                        <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                                        <div class="help-block text-right"><a href="">Forgot the password ?</a></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                        <input type="checkbox"> keep me logged-in
                                                        </label>
                                                    </div>
                                            </form>
                                        </div>
                                        <div class="bottom text-center">
                                            New here ? <a href="#"><b>Join Us</b></a>
                                        </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                </div><!-- /.nav-collapse -->
            </nav>
            </div>
        </div>  
    </header><!--/header-->
        <!-- Navbar End-->
<?php
}
?>